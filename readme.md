Анализируем ИМЯ+Отчество+Фамилия (в произвольном порядке)
возвращаем dict
Примеры:
#print(analyzeName(u"Вячеслав Леонидович Куницкий"))
>>> {'middle': 'Леонидович', 'secondname': 'Куницкий', 'gender': 'MALE', 'firstname': 'Вячеслав'}

#print(analyzeName(u"Hans-Josef Fritschi"))
>>> {'middle': None, 'secondname': 'Fritschi', 'gender': 'UNDEFINED', 'firstname': 'Hans-Josef'}
#print(analyzeName(u"Rolf Sanger"))
 {'middle': None, 'secondname': 'Sanger', 'gender': 'MALE', 'firstname': 'Rolf'}