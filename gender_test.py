# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 10:22:50 2016

@author: Veaceslav Kunitki
"""
import requests
import json

key="DyJ7Xj8ROoGUiUWlxUjUVSK"
otcestvo=["ович","евич","ич","овна","евна","ична","инична"]
def getOtcestvo(name):
    words = name.split()
    for end in otcestvo:   
        for word in words:
            if word.endswith(end):
                return word

def mytrim(str):
    if str!=None:
        return str.lstrip()
def analyzeName(name):
    Allwords = name.split()
    middle=None
    name2=name.replace(" ","&nbsp;")
    r = requests.get("https://api.name2gender.com/gender?key="+key+"&name="+name2)
    answer=r.content.decode("utf-8") 
    #print (answer)
    js=json.loads(""+answer)
    secondname=name.replace(js["name"],"")
    words = secondname.split()
    if (len(words)==1):
        secondname=secondname.replace(" ","")    
    if (len(Allwords)==3):
        middle=getOtcestvo(name)
        if middle!=None:
            secondname=secondname.replace(middle,"")    
            secondname=secondname.replace(" ","")    
        
    r={"gender":js["gender"],"firstname":mytrim(js["name"]),"secondname":mytrim(secondname),"middle":mytrim(middle)}        
    #print (r)
    return r




#print(analyzeName(u"Heike Jäckel"))
#print(analyzeName(u"Вячеслав Леонидович Куницкий"))
#print(analyzeName(u"Elke Johanna Franz"))

